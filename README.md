### Overview
- This program acts like in memory search engine and loads all json data at start the program and returns full value text search results
- Search index uses Hash data structure in order to maintain performance of the app close to O(1)
- Search indexer uses java generics and reflections in order work with any json schema without nested array values
- Indexer code is available under util.indexer package

###Tools required to run this program:
* minimum JDK8 Version
* maven 3.6 or higher

### How to build and run the program
Step 1: model pojo's are generated from json using following command. In order to new Data model please refer schema folder under resources

``` mvn generate-sources```

Step2: Execute below command in order to create jar file

```mvn package```

Step3: Export following environment variable pointing root folder for data sources

Eg:
``` ZD_SEARCH_DATA_ROOT=/Users/jagadish_av/Desktop/zd/project/zdsearch/data```

Note: 
- Path might look different based on your OS and also if they above varialbe is not exported then program will not start
- Ensure sub Root folder name matches schema.json file name provided under resources folder
Eg:
```
/data/User/*.json match User.json file name under resources
```

Step 4: Execute zdsearch-1.0-SNAPSHOT-jar-with-dependencies.jar generated under target folder

```java -jar target/ java -jar zdsearch-1.0-SNAPSHOT-jar-with-dependencies.jar ```

Note: ensure you choose jar-with-dependencies for executing. It is basically a fat jar with all dependencies so that it can run anywhere without any dependency management

###Assumptions
* No duplicate data should be present in json data files otherwise search index will index and return those duplicates
* Root folder for particular json schema data should match with the file name mentioned under resource folder
  Eg:
  resouces/schema/User.json should match /data/User/*.json
* This program does full value search only
* Model can contain only 1 level of arrays for multi-level programs needs modification
  Eg: please refer domain_names section below.
```
  {
    "_id": 101,
    "url": "http://initech.zendesk.com/api/v2/organizations/101.json",
    "external_id": "9270ed79-35eb-4a38-a46f-35725197ea8d",
    "name": "Enthaze",
    "domain_names": [
      "kage.com",
      "ecratic.com",
      "endipin.com",
      "zentix.com"
    ],
    "created_at": "2016-05-21T11:10:28 -10:00",
    "details": "MegaCorp",
    "shared_tickets": false,
    "tags": [
      "Fulton",
      "West",
      "Rodriguez",
      "Farley"
    ]
  }
```
* Only error logs are collected in log file for time being and log file is located with file name zdsearch-application.log
* Ensure size of json data provided cannot exceed system memory provided to the program