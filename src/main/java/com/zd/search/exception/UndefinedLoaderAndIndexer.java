package com.zd.search.exception;

public class UndefinedLoaderAndIndexer extends Exception{
    public UndefinedLoaderAndIndexer(String errorMessage){
        super(errorMessage);
    }
}
