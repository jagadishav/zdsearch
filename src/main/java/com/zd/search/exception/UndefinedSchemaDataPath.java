package com.zd.search.exception;

public class UndefinedSchemaDataPath extends  Exception{
    public UndefinedSchemaDataPath(String errorMessage){
        super(errorMessage);
    }
}
