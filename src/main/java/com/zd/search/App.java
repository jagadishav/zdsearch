package com.zd.search;

import com.zd.search.controller.SearchController;
import com.zd.search.exception.UndefinedLoaderAndIndexer;
import com.zd.search.exception.UndefinedSchemaDataPath;
import com.zd.search.repository.SearchRepository;
import com.zd.search.service.SearchService;
import com.zd.search.ui.UserScreen;
import org.apache.log4j.Logger;

import java.io.IOException;

public class App {
    final static Logger logger = Logger.getLogger(App.class);

    public static void main(String[] args) {
        try {
            //Initial and index data in memory
            SearchRepository searchRepository = new SearchRepository();
            SearchService searchService = new SearchService(searchRepository);
            SearchController searchController = new SearchController(searchService);
            searchController.loadAndIndexSchemaData();

            //Create UI screens
            UserScreen userScreen = new UserScreen(searchController);
            userScreen.loadUIScreen();

        } catch (UndefinedSchemaDataPath e) {
            logger.error("Undefined environment variable ZD_SEARCH_DATA_ROOT", e);
        } catch (IOException ioException){
            logger.error("Unable to load schema Data",ioException);
        } catch (UndefinedLoaderAndIndexer undefinedLoaderAndIndexer){
            logger.error("Undefined Loader or Indexer",undefinedLoaderAndIndexer);
        } catch (Exception e){
            logger.error("Runtime error occurred ",e);
        }
    }
}