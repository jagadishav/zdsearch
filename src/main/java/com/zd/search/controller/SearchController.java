package com.zd.search.controller;

import com.zd.search.exception.UndefinedLoaderAndIndexer;
import com.zd.search.exception.UndefinedSchemaDataPath;
import com.zd.search.service.SearchService;

import java.io.IOException;

public class SearchController {
    private final SearchService searchService;
    public SearchController(SearchService searchService)  {
        this.searchService = searchService;
    }

    public void loadAndIndexSchemaData() throws UndefinedSchemaDataPath, UndefinedLoaderAndIndexer, IOException {
        this.searchService.loadAndIndexSchemaDataa();
    }

    public void printListOfSearchableFields() {
        this.searchService.printListOfSearchableFields();
    }

    public void printSearchResultsFromSchemaData(String option, String fieldName, String fieldValue){
        this.searchService.printSearchResultsFromSchemaData(option, fieldName, fieldValue);
    }
}
