package com.zd.search.service;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.zd.search.exception.UndefinedLoaderAndIndexer;
import com.zd.search.exception.UndefinedSchemaDataPath;
import com.zd.search.repository.SearchRepository;
import com.zd.search.util.indexer.SchemaDataIndexer;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class SearchService {
    private final SearchRepository searchRepository;

    public SearchService(SearchRepository searchRepository){
        this.searchRepository = searchRepository;
    }

    public void loadAndIndexSchemaDataa() throws UndefinedLoaderAndIndexer, IOException, UndefinedSchemaDataPath {
        this.searchRepository.loadAndIndexSchemaData();
    }

    public void printListOfSearchableFields(){
        HashMap schemaDataIndexMapper =searchRepository.getListOfSearchableFields();
        Set<String> keys = schemaDataIndexMapper.keySet();
        for(String key:keys){
            System.out.println("-------------------------------------------------------");
            System.out.println("Search "+key+"s with");

            SchemaDataIndexer<?> map = (SchemaDataIndexer) schemaDataIndexMapper.get(key);

            List<String> searchableFields = map.getListOfSearchableFieldsOnSchema();
            for(String searchableField: searchableFields){
                System.out.println(searchableField);
            }
        }
    }

    public void printSearchResultsFromSchemaData(String option, String fieldName, String fieldValue){
        if(fieldValue.length() == 0){
            fieldValue = null;
        }

        switch (option){
            case "1":
                option="USER";
                break;
            case "2":
                option="TICKET";
                break;
            case "3":
                option="ORGANIZATION";
                break;
        }
        List<?> result = this.searchRepository.getSearchResultsFromSchemaData(option, fieldName, fieldValue);

        System.out.println("Searching "+ option+"S for "+ fieldName+ " with a value of "+ fieldValue);
        if(result == null){
            System.out.println("No results found");
        } else{
            for (Object o : result) {
                printSearchResult(o);
                System.out.println("------------------------------------------");
            }
        }
    }

    private void printSearchResult(Object obj){
        Field[] fields = obj.getClass().getDeclaredFields();

        for(Field field: fields){
            if(isSchemaProperty(field)){
                System.out.println(invokeGetter(obj, field.getName()));
            }
        }
    }

    private boolean isSchemaProperty(Field field) {
        Annotation[] annotations = field.getDeclaredAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof JsonProperty) {
                return true;
            }
        }
        return false;
    }

    private Object invokeGetter(Object obj, String variableName) {
        try {
            PropertyDescriptor pd = new PropertyDescriptor(variableName, obj.getClass());
            Method getter = pd.getReadMethod();
            Object fieldValue = getter.invoke(obj);

            if (fieldValue != null) {
                return fieldValue;
            }
        } catch (IllegalAccessException | IntrospectionException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }


}

