package com.zd.search.ui;

import com.zd.search.controller.SearchController;

import java.io.IOException;
import java.util.Scanner;

public class UserScreen {
    private final Scanner console = new Scanner(System.in);
    private final SearchController searchController;

    public UserScreen(SearchController searchController){
        this.searchController = searchController;
    }

    public void loadUIScreen() throws IOException {
        System.out.println("Welcome to Zendesk Search");
        System.out.println("Type 'quit' to exit any time, Press 'Enter' to continue\n\n");
        while(true){
            System.out.println("\t\tSelect search options");
            System.out.println("\t\t * Press 1 to search Zendesk");
            System.out.println("\t\t * Press 2 to view a list of searchable Fields");
            System.out.println("\t\t * Type 'quit' to exit");


            String option = console.nextLine();
            if(option.toUpperCase().contentEquals("QUIT")){
                break;
            }

            switch (option){
                case "1":
                    this.provideSearchOptions();
                    break;
                case "2":
                    searchController.printListOfSearchableFields();
                    break;
                default:
                    System.out.println("Please enter a valid option shown below:");
            }
        }
    }

    private void provideSearchOptions() {
        System.out.println("select 1) Users or 2) Tickets 3) Organizations");
        String option = console.nextLine();

        if(option.equals("1") || option.equals("2") || option.equals("3")){
            System.out.println("Enter search term");
            String searchField = console.nextLine();
            System.out.println("Enter search value");
            String searchValue = console.nextLine();

            this.searchController.printSearchResultsFromSchemaData(option, searchField,searchValue);
        }else{
            System.out.println("Please enter a valid option shown below:");
        }
    }
}
