package com.zd.search.repository;

import com.zd.search.exception.UndefinedLoaderAndIndexer;
import com.zd.search.exception.UndefinedSchemaDataPath;
import com.zd.search.model.Organization;
import com.zd.search.model.Ticket;
import com.zd.search.model.User;
import com.zd.search.util.DataFileUtil;
import com.zd.search.util.dataloader.OrganizationJsonDataLoaderIntoMemoryImpl;
import com.zd.search.util.dataloader.TicketJsonDataLoaderIntoMemoryImpl;
import com.zd.search.util.dataloader.UserJsonDataLoaderIntoMemoryImpl;
import com.zd.search.util.indexer.SchemaDataIndexer;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchRepository {
    private HashMap<String, SchemaDataIndexer> schemaDataIndexMapper = new HashMap<>();

    public HashMap getListOfSearchableFields(){
        return this.schemaDataIndexMapper;
    }

    public void loadAndIndexSchemaData() throws UndefinedLoaderAndIndexer, IOException, UndefinedSchemaDataPath {
        String rootPath = System.getenv("ZD_SEARCH_DATA_ROOT");
        if (rootPath == null) {
            throw new UndefinedSchemaDataPath("Undefined schema data path");
        }

        List<String> listOfAllSchema = DataFileUtil.listAllSchemas(rootPath);

        for(String schemaName: listOfAllSchema){
            SchemaDataIndexer schemaDataIndexer = this.loadAndIndexSchemaDataMapper(rootPath, schemaName);
            this.schemaDataIndexMapper.put(schemaName.toUpperCase(),schemaDataIndexer);
        }
    }

    public List getSearchResultsFromSchemaData(String schemaName, String fieldName, String fieldValue){
        SchemaDataIndexer schemaDataIndexer = this.schemaDataIndexMapper.get(schemaName);

        Map fieldIndexer = schemaDataIndexer !=null ? schemaDataIndexer.getSchemaFieldIndexer() : null;
        Map dataIndexer =  fieldIndexer !=null ? (Map) fieldIndexer.get(fieldName):null;
        return dataIndexer !=null ? (List) dataIndexer.get(fieldValue) : null;
    }

    public SchemaDataIndexer loadAndIndexSchemaDataMapper(String rootPath, String schemaName) throws UndefinedLoaderAndIndexer, IOException {
        switch (schemaName.toUpperCase()){
            case "USER":
                return loadAndIndexUserSchemaData(rootPath,schemaName);
            case "TICKET":
                return loadAndIndexTicketSchemaData(rootPath,schemaName);
            case "ORGANIZATION":
                return loadAndIndexOrganizationSchemaData(rootPath, schemaName);
            default:
                throw new UndefinedLoaderAndIndexer("Loader and index is not defined for provided schema");
        }
    }

    public SchemaDataIndexer loadAndIndexUserSchemaData(String rootPath, String schemaName) throws IOException {
        List<String> schemaDataPaths = DataFileUtil.listAllEntityFiles(rootPath, schemaName);
        UserJsonDataLoaderIntoMemoryImpl userDataLoader = new UserJsonDataLoaderIntoMemoryImpl(schemaDataPaths);
        List<User> users = userDataLoader.loadJsonDataIntoMemory();

        SchemaDataIndexer<User> userSchemaDataIndexer = new SchemaDataIndexer<>(User.class);
        userSchemaDataIndexer.indexSchemaData(users);

        return userSchemaDataIndexer;
    }

    public SchemaDataIndexer loadAndIndexOrganizationSchemaData(String rootPath, String schemaName) throws IOException {
        List<String> schemaDataPaths = DataFileUtil.listAllEntityFiles(rootPath, schemaName);
        OrganizationJsonDataLoaderIntoMemoryImpl organizationDataLoader = new OrganizationJsonDataLoaderIntoMemoryImpl(schemaDataPaths);
        List<Organization> organizations = organizationDataLoader.loadJsonDataIntoMemory();

        SchemaDataIndexer<Organization> organizationSchemaDataIndexer = new SchemaDataIndexer<>(Organization.class);
        organizationSchemaDataIndexer.indexSchemaData(organizations);

        return organizationSchemaDataIndexer;
    }

    private SchemaDataIndexer loadAndIndexTicketSchemaData(String rootPath, String schemaName) throws IOException {
        List<String> schemaDataPaths = DataFileUtil.listAllEntityFiles(rootPath, schemaName);
        TicketJsonDataLoaderIntoMemoryImpl organizationDataLoader = new TicketJsonDataLoaderIntoMemoryImpl(schemaDataPaths);
        List<Ticket> tickets = organizationDataLoader.loadJsonDataIntoMemory();

        SchemaDataIndexer<Ticket> ticketSchemaDataIndexer = new SchemaDataIndexer<>(Ticket.class);
        ticketSchemaDataIndexer.indexSchemaData(tickets);

        return ticketSchemaDataIndexer;
    }
}
