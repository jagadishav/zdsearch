package com.zd.search.util.indexer;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class SchemaDataIndexer<T> {
    private final HashMap<String, HashMap<String, ArrayList<T>>> schemaFieldIndexer = new HashMap<>();
    private final Class<T> schemaClassObj;

    public SchemaDataIndexer(Class<T> schemaClassObj) {
        this.schemaClassObj = schemaClassObj;
        this.createSchemaFieldIndex();
    }

    public HashMap<String, HashMap<String, ArrayList<T>>> getSchemaFieldIndexer() {
        return schemaFieldIndexer;
    }

    public List<String> getListOfSearchableFieldsOnSchema() {
        List<String> searchFields = new ArrayList<>();
        Annotation[] annotations = this.schemaClassObj.getAnnotations();

        for (Annotation annotation: annotations){
            if(annotation instanceof JsonPropertyOrder){
                JsonPropertyOrder jsonPropertyOrder = (JsonPropertyOrder)annotation;
                String[] values = jsonPropertyOrder.value();
                Collections.addAll(searchFields, values);
            }
        }

        return searchFields;
    }

    private void createSchemaFieldIndex() {
        Field[] fields = this.schemaClassObj.getDeclaredFields();
        for (Field field : fields) {
            String fieldName = getJsonFieldName(field);

            if (fieldName != null) {
                schemaFieldIndexer.put(fieldName, new HashMap<>());
            }
        }
    }

    public void indexSchemaData(List<T> schemaData) {
        Field[] fields = this.schemaClassObj.getDeclaredFields();

        //Data index by Field
        for (T t : schemaData) {
            for (Field field : fields) {
                String keyName = this.getJsonFieldName(field);

                if(keyName !=null){
                    HashMap<String, ArrayList<T>> schemaDataIndexer = schemaFieldIndexer.get(keyName);
                    Object fieldValueOnObject = invokeGetter(t, field.getName());

                    if(fieldValueOnObject instanceof List<?>){
                        for(int i=0; i<((List<?>) fieldValueOnObject).size(); i++){
                            updateSchemaUnitData(schemaDataIndexer, ((List<?>) fieldValueOnObject).get(i), t);
                        }
                    } else {
                        updateSchemaUnitData(schemaDataIndexer, fieldValueOnObject, t);
                    }
                }
            }
        }
    }

    private void updateSchemaUnitData(HashMap<String, ArrayList<T>> schemaDataIndexer, Object fieldValueOnObject, T t){
        String fieldValue = null;

        if(fieldValueOnObject!=null){
            fieldValue = fieldValueOnObject.toString();
        }

        ArrayList<T> unitData = schemaDataIndexer.get(fieldValue);

        if (unitData != null) {
            unitData.add(t);
        } else {
            ArrayList<T> data = new ArrayList<>();
            data.add(t);
            schemaDataIndexer.put(fieldValue, data);
        }
    }

    private String getJsonFieldName(Field field) {
        Annotation[] annotations = field.getDeclaredAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof JsonProperty) {
                return ((JsonProperty) annotation).value();
            }
        }
        return null;
    }


    private Object invokeGetter(Object obj, String variableName) {
        try {
            PropertyDescriptor pd = new PropertyDescriptor(variableName, obj.getClass());
            Method getter = pd.getReadMethod();
            Object fieldValue = getter.invoke(obj);

            if (fieldValue != null) {
                return fieldValue;
            }
        } catch (IllegalAccessException | IntrospectionException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }
}
