package com.zd.search.util.dataloader;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zd.search.model.Ticket;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TicketJsonDataLoaderIntoMemoryImpl implements DataLoaderIntoMemory{
    private final List<String> paths;

    public TicketJsonDataLoaderIntoMemoryImpl(List<String> paths) {
        this.paths = paths;
    }

    @Override
    public List<Ticket> loadJsonDataIntoMemory() throws IOException {
        List<Ticket> masterList = new ArrayList<>();
        for(String path: this.paths){
            File file = new File(path);
            ObjectMapper mapper = new ObjectMapper();

            List<Ticket> tickets = Arrays.asList(mapper.readValue(file, Ticket[].class));
            masterList.addAll(tickets);
        }
        return masterList;
    }
}
