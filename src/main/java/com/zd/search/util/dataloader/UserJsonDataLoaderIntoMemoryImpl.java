package com.zd.search.util.dataloader;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zd.search.model.User;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserJsonDataLoaderIntoMemoryImpl implements DataLoaderIntoMemory{
    private final List<String> paths;

    public UserJsonDataLoaderIntoMemoryImpl(List<String> paths) {
        this.paths = paths;
    }

    @Override
    public List<User> loadJsonDataIntoMemory() throws IOException {
        List<User> masterList = new ArrayList<>();
        for(String path: this.paths){
            File file = new File(path);
            ObjectMapper mapper = new ObjectMapper();

            List<User> users =  Arrays.asList(mapper.readValue(file, User[].class));
            masterList.addAll(users);
        }
        return masterList;
    }
}
