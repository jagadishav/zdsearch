package com.zd.search.util.dataloader;

import java.io.IOException;
import java.util.List;

public  interface DataLoaderIntoMemory {
    public  List<?> loadJsonDataIntoMemory() throws IOException;
}
