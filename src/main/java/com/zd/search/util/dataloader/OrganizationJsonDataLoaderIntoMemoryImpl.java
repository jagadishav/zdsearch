package com.zd.search.util.dataloader;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zd.search.model.Organization;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OrganizationJsonDataLoaderIntoMemoryImpl implements DataLoaderIntoMemory{
    private final List<String> paths;

    public OrganizationJsonDataLoaderIntoMemoryImpl(List<String> paths) {
        this.paths = paths;
    }

    @Override
    public List<Organization> loadJsonDataIntoMemory() throws IOException {
        List<Organization> masterList = new ArrayList<>();
        for(String path: paths){
            File file = new File(path);
            ObjectMapper mapper = new ObjectMapper();

            List<Organization>  organizations= Arrays.asList(mapper.readValue(file, Organization[].class));
            masterList.addAll(organizations);
        }
        return masterList;
    }
}
