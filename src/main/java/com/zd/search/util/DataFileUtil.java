package com.zd.search.util;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

public class DataFileUtil {

    public static List<String> listAllSchemas(String path) {
        File  file= new File(path);
        ArrayList<String> listOfAllEntities = new ArrayList<>();
        File[] directories = file.listFiles(File::isDirectory);
        String rootPath = file.getAbsolutePath();

        for (File dir : directories) {
            listOfAllEntities.add(dir.getAbsolutePath().replace(rootPath + "/", ""));
        }

        return listOfAllEntities;
    }

    public static List<String> listAllEntityFiles(String rootPath, String entityName) {
        File rootPathObj = new File(rootPath);
        List<String> absoluteFilePaths = new ArrayList<>();
        File[] files = new File(rootPathObj.getAbsolutePath()+"/"+entityName).listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".json");
            }
        });

        for(File file:files){
            absoluteFilePaths.add(file.getAbsolutePath());
        }

        return absoluteFilePaths;
    }
}
