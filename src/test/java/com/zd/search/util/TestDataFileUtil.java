package com.zd.search.util;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class TestDataFileUtil {

    @Test
    public void testListAllSchemas(){
        File file = new File("src/test/resources");
        String absolutePath = file.getAbsolutePath();

        Assert.assertTrue(DataFileUtil.listAllSchemas(absolutePath).size() == 3);
    }

    //If incorrect path is provided runtime error should occur
    @Test(expected = RuntimeException.class)
    public void testListAllSchemasFailure(){
        File file = new File("src/test/resource");
        String absolutePath = file.getAbsolutePath();

        DataFileUtil.listAllSchemas(absolutePath);
    }

    @Test
    public void testListAllSchemaPaths(){
        File file = new File("src/test/resources");
        String absolutePath = file.getAbsolutePath();

        Assert.assertTrue(DataFileUtil.listAllEntityFiles(absolutePath,"User").size() == 2);
    }

    //If incorrect path is provided runtime error should occur
    @Test(expected = RuntimeException.class)
    public void testListAllSchemaFilesFailure(){
        File file = new File("src/test/resource");
        String absolutePath = file.getAbsolutePath();

        DataFileUtil.listAllEntityFiles(absolutePath, "User");
    }


}
