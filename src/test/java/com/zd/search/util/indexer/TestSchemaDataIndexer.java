package com.zd.search.util.indexer;

import com.zd.search.model.User;
import com.zd.search.util.DataFileUtil;
import com.zd.search.util.dataloader.UserJsonDataLoaderIntoMemoryImpl;
import com.zd.search.util.indexer.SchemaDataIndexer;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TestSchemaDataIndexer {
    SchemaDataIndexer<User> schemaDataIndexer = new SchemaDataIndexer<>(User.class);
    File file = new File("src/test/resources");
    String absolutePath = file.getAbsolutePath();
    List<String> paths = DataFileUtil.listAllEntityFiles(absolutePath,"User");
    UserJsonDataLoaderIntoMemoryImpl userJsonDataLoaderIntoMemory = new UserJsonDataLoaderIntoMemoryImpl(paths);
    List<User> users = userJsonDataLoaderIntoMemory.loadJsonDataIntoMemory();

    public TestSchemaDataIndexer() throws IOException {
        //IO exception is associated Data loader
        //and will be tested as part of that test
    }

    @Test
    public void testNumberUserSchemaFields() throws IOException {
        schemaDataIndexer.indexSchemaData(users);
        Assert.assertTrue(schemaDataIndexer.getSchemaFieldIndexer().size() == 19) ;
    }

    @Test
    public void testNumberUserDataIndexed() throws IOException {
        schemaDataIndexer.indexSchemaData(users);
        Assert.assertTrue(schemaDataIndexer.getSchemaFieldIndexer().get("_id").size() == 75) ;
    }

    @Test
    public void testShouldNotFailOnZeroData() {
        List<User> users = new ArrayList<>();

        schemaDataIndexer.indexSchemaData(users);
        Assert.assertTrue(schemaDataIndexer.getSchemaFieldIndexer().get("_id").size() == 0) ;
    }

}

