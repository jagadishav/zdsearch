package com.zd.search.util.dataloader;

import com.zd.search.util.DataFileUtil;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class TestDataLoader {

    //Cross marshalling should fail data load into memory
    @Test(expected = IOException.class)
    public void testDataloaderException() throws IOException {
        File file = new File("src/test/resources");
        String absolutePath = file.getAbsolutePath();
        List<String> paths = DataFileUtil.listAllEntityFiles(absolutePath,"Ticket");

        UserJsonDataLoaderIntoMemoryImpl userJsonDataLoaderIntoMemory = new UserJsonDataLoaderIntoMemoryImpl(paths);
        userJsonDataLoaderIntoMemory.loadJsonDataIntoMemory();
    }

    @Test
    public void testSuccessFullDataLoad() throws IOException {
        File file = new File("src/test/resources");
        String absolutePath = file.getAbsolutePath();
        List<String> paths = DataFileUtil.listAllEntityFiles(absolutePath,"User");

        UserJsonDataLoaderIntoMemoryImpl userJsonDataLoaderIntoMemory = new UserJsonDataLoaderIntoMemoryImpl(paths);
        userJsonDataLoaderIntoMemory.loadJsonDataIntoMemory();
    }


}
